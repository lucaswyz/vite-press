module.exports = {
  title: '我',
  description: '我的',
  lang: 'en-US',
  base: '/',
  themeConfig: {
    algolia: {
      apiKey: 'your_api_key',
      indexName: 'index_name',
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'VitePress', link: '/vitepress/' },
      { text: 'Git', link: '/git/' },
      { text: '.NET', link: '/net/' },
      { text: 'baidu', link: 'https://www.baidu.com/' }
    ]
  },

}