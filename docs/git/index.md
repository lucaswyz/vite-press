---
title: Git
sidebar: auto
---
## 创建

>- git init 创建版本库（仓库）

> - git add <kbd>file</kbd> 把文件/修改添加到暂存区 add . 添加所有文件/修改到暂存区
> - git status 命令可以让我们时刻掌握仓库当前的状态
> - git reset <kbd>file</kbd> 撤销添加 暂存区撤回到工作区
> - git restore --staged <kbd>file</kbd>放弃暂存区中的更改,将暂存区修改回退到工作区
> - git commit -m <kbd>message</kbd> 把暂存区文件提交到版本库(当前分支) -a 自动 add
> - git reset --soft HEAD^ 回退上一次提交

## 分支
> - git branch 列出分支 <kbd>name</kbd> 创建分支
> - git checkout 切换分支 -b 创建并切换到分支
> - git branch -d <kbd>name</kbd> 删除分支

> - git log 显示从最近到最远的提交日志 --pretty=oneline 参数 简化信息
> - git reset --hard <kbd>commit id</kbd> 上一个版本就是 HEAD^ ,上 100 个版本写成 HEAD~100 -- HEAD 是指针
> - git reflog 查看版本记录
> - git diff <kbd>file</kbd>命令可以查看工作区和版本库里面最新版本的区别 --git diff HEAD -- <kbd>file</kbd> 同效果
> - git checkout -- <kbd>file</kbd> 把<kbd>file</kbd>文件在工作区的修改全部撤销
> - git reset HEAD <kbd>file</kbd> 将暂存区修改回退到工作区
> - git rm <kbd>file</kbd> 在暂存区删除文件
> - git reset --soft HEAD~1 回退一个版本并将修改回退到暂存区

> - git config --global user.name 查看全局的 git 名称，可修改
> - git config --global user.email 查看全局的 git 邮箱，可修改
